 import java.sql.Connection;
 import java.sql.DriverManager;
 import java.sql.ResultSet;
 import java.sql.SQLException;
 import java.sql.Statement;

    public class Alumno
    {
      public static void main(String[] args)
      {
        Connection connection = null;
        try
        {
          System.out.println("Número de dni: ");
          String dni;
          String nif;
          Boolean Not_Found = true;//sin dni, no se puede entrar
          dni = System.console().readLine();
          // create a database connection
          connection = DriverManager.getConnection("jdbc:sqlite:BBDD/BBDD_pf.db");
          Statement statement = connection.createStatement();
          ResultSet rs = statement.executeQuery("select * from Entrega");

	  System.out.println("----------------------------------------------");
	  System.out.println("Resultados del alumn@ cuyo dni es " + dni + " son: ");
	  while(rs.next()){
	  	nif  = rs.getString("nif");
	  	if (dni.compareTo(nif) == 0){
	  	//if (dni == rs.getString("nif")){
	  	//if (dni.compareTo(nif) != 0){
			//read the result set
			System.out.println(" ");
			System.out.println("Nombre de la práctica: " + rs.getString("nombre"));
			System.out.println("DNI del alumno: " + rs.getString("nif"));
			//quiza no necesario DNI porque es la contraseña
			//System.out.println("Año: " + rs.getInt("ano"));
			System.out.println("URL_repo: " + rs.getString("url_repo"));
			System.out.println("Nota: " + rs.getFloat("nota"));
			//System.out.println("Comentario: " + rs.getString("comentario"));
			System.out.println("----------------------------------------------");
			//Entregas: Samuel, Juan, Elena -> 3 grupos distintos
			Not_Found = false;
			//Not_Found = true;//debe ser false, pero para probar si llega al if siguiente
			//System.out.println(Not_Found);//bien, porque devuelve true
		}
	  }
	  if (Not_Found){//!Not_Found->caso contrario
		  System.out.println(" ");
		  System.out.println("Número de dni no válido. Por favor, introduzca un número válido.");
	  }
        }
        catch(SQLException e)
        {
          // if the error message is "out of memory",
          // it probably means no database file is found
          System.err.println(e.getMessage());
        }
        finally
        {
          try
          {
            if(connection != null)
              connection.close();
          }
          catch(SQLException e)
          {
            // connection close failed.
            System.err.println(e.getMessage());
          }
        }
      }
    }
