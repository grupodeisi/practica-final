 import java.sql.Connection;
 import java.sql.DriverManager;
 import java.sql.ResultSet;
 import java.sql.SQLException;
 import java.sql.Statement;

    public class Listado_notas
    {
      public static void main(String[] args)
      {
        Connection connection = null;
        try
        {
          // create a database connection
          connection = DriverManager.getConnection("jdbc:sqlite:BBDD_pf.db");
          Statement statement = connection.createStatement();
          ResultSet rs = statement.executeQuery("select * from Entrega");

	  System.out.println("   ");
	  System.out.println("Listado de calificaciones:");
	  System.out.println("   ");
	  while(rs.next()) {
	        //read the result set
	  	System.out.println("DNI del alumno: " + rs.getString("nif"));
		System.out.println("Nombre de la práctica: " + rs.getString("nombre"));
		System.out.println("URL_repo: " + rs.getString("url_repo"));
		System.out.println("Nota: " + rs.getFloat("nota"));
		System.out.println("----------------------------------");
	  }
        }
        catch(SQLException e)
        {
          // if the error message is "out of memory",
          // it probably means no database file is found
          System.err.println(e.getMessage());
        }
        finally
        {
          try
          {
            if(connection != null)
              connection.close();
          }
          catch(SQLException e)
          {
            // connection close failed.
            System.err.println(e.getMessage());
          }
        }
      }
    }
