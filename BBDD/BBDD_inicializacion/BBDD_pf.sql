PRAGMA foreign_keys = ON;
--SET Sql_Mode = 'strict_all_tables';

CREATE TABLE IF NOT EXISTS "Alumno" (nif CHAR(10) NOT NULL, nombre CHAR (30) NOT NULL, 
			ano INT NOT NULL,
			PRIMARY KEY (nif));
			
CREATE TABLE IF NOT EXISTS "Profesor" (nif CHAR(10) NOT NULL, nombre CHAR (30) NOT NULL,
			PRIMARY KEY (nif));

CREATE TABLE IF NOT EXISTS "Asignatura" (id_asignatura INT NOT NULL, 
			nombre CHAR (30) NOT NULL, ano INT NOT NULL, 
			PRIMARY KEY (id_asignatura));

CREATE TABLE IF NOT EXISTS "Practicas" (nombre CHAR (30) NOT NULL, 
			id_asignatura INT NOT NULL,
			FOREIGN KEY (id_asignatura) REFERENCES Asignatura (id_asignatura) ON DELETE CASCADE,
			PRIMARY KEY (nombre));

CREATE TABLE IF NOT EXISTS "Entrega" (nif CHAR(10) NOT NULL, nombre CHAR (30) NOT NULL,
			url_repo CHAR(200), nota FLOAT NOT NULL, 
			PRIMARY KEY (url_repo),
			FOREIGN KEY (nif) REFERENCES Alumno (nif) ON DELETE CASCADE,
			FOREIGN KEY (nombre) REFERENCES Practicas (nombre) ON DELETE CASCADE);

CREATE TABLE IF NOT EXISTS "asignatura_profesor" (nif CHAR(10) NOT NULL,
			id_asignatura INT NOT NULL,
			FOREIGN KEY (nif) REFERENCES Profesor (nif) ON DELETE CASCADE,
			FOREIGN KEY (id_asignatura) REFERENCES Asignatura (id_asignatura));
		
CREATE TABLE IF NOT EXISTS "asignatura_alumno" (nif CHAR(10) NOT NULL,
			id_asignatura INT NOT NULL,
			FOREIGN KEY (nif) REFERENCES Alumno (nif),
			FOREIGN KEY (id_asignatura) REFERENCES Asignatura (id_asignatura));
			

--Introducimos valores por defecto en la BBDD
INSERT INTO "Alumno" (nif, nombre, ano) VALUES (123456, "Amelia", 2017);
INSERT INTO "Alumno" (nif, nombre, ano) VALUES (654321, "Paula", 2017);
INSERT INTO "Alumno" (nif, nombre, ano) VALUES (001122, "Samuel", 2016);
INSERT INTO "Alumno" (nif, nombre, ano) VALUES (034455, "Juan", 2019);

INSERT INTO "Profesor" (nif, nombre) VALUES ("ABCDEF", "Pedro");
INSERT INTO "Profesor" (nif, nombre) VALUES ("FEDCBA", "Antonio");

INSERT INTO "Asignatura" (id_asignatura, nombre, ano) VALUES (1, "ISI", 2021);
INSERT INTO "asignatura_profesor" (nif, id_asignatura) VALUES ("ABCDEF", 1);
INSERT INTO "asignatura_alumno" (nif, id_asignatura) VALUES (123456, 1);
INSERT INTO "asignatura_alumno" (nif, id_asignatura) VALUES (654321, 1);
INSERT INTO "asignatura_alumno" (nif, id_asignatura) VALUES (001122, 1);

INSERT INTO "Asignatura" (id_asignatura, nombre, ano) VALUES (02, "PDAC", 2021);
INSERT INTO "asignatura_profesor" (nif, id_asignatura) VALUES ("FEDCBA", 2);
INSERT INTO "asignatura_alumno" (nif, id_asignatura) VALUES (123456, 2);
INSERT INTO "asignatura_alumno" (nif, id_asignatura) VALUES (654321, 2);
INSERT INTO "asignatura_alumno" (nif, id_asignatura) VALUES (034455, 2);

INSERT INTO "Practicas" (nombre, id_asignatura) VALUES ("PrácticaISI 1", 1);
INSERT INTO "Practicas" (nombre, id_asignatura) VALUES ("PrácticaISI 2", 1);
INSERT INTO "Practicas" (nombre, id_asignatura) VALUES ("PrácticaPDAC 1", 2);
INSERT INTO "Practicas" (nombre, id_asignatura) VALUES ("PrácticaPDAC 2", 2);

INSERT INTO "Entrega" (nif, nombre, url_repo, nota) VALUES (001122, "PrácticaPDAC 1", "prueba12345", 9.5);
INSERT INTO "Entrega" (nif, nombre, url_repo, nota) VALUES (001122, "PrácticaISI 1", "prueba54321", 8.3);
INSERT INTO "Entrega" (nif, nombre, url_repo, nota) VALUES (654321, "PrácticaPDAC 1", "prueb3333a12345", 6.0);
INSERT INTO "Entrega" (nif, nombre, url_repo, nota) VALUES (654321, "PrácticaISI 1", "prueb33333a54321", 4.0);






