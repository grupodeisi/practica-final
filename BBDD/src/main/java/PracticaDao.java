import java.sql.*;
import java.util.ArrayList;
import java.util.List;


public class PracticaDao {

    private static Connection c;

    public PracticaDao() {
        try {
            if(c!=null) return;

            c = DriverManager.getConnection("jdbc:sqlite:BBDD_pf.db");	// Mi aplicación java se comunicará con squlite utilizando la librería jdbc 
            									// usando una connexion TPC
            c.setAutoCommit(false);			// Usar transacciones: que la lista de sentencias no se ejecute hasta el commit de la linea 20.
							// O se ejecuten las dos o ninguna
            c.commit();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public List<Practica> all() {
        List<Practica> allPracticas = new ArrayList<Practica>();

        try {
            PreparedStatement ps = c.prepareStatement("select * from Practicas");
            ResultSet rs = ps.executeQuery();

            while(rs.next()) {
                String name = rs.getString("nombre");
                int id_asignatura = rs.getInt("id_asignatura");
                allPracticas.add(new Practica(name, id_asignatura));
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            return allPracticas;
        }
    }

    public void save(Practica p) {
        try {
            PreparedStatement ps = c.prepareStatement("INSERT INTO Practicas (nombre, id_asignatura) VALUES (?, ?)");
            ps.setString(1, p.getNombre());		// Inyecta en la interrogación (?) 1 un string
            ps.setInt(2, p.getIdAsignatura());	// Inyecta en la interrogación (?) 2 un integer
            ps.execute();
            c.commit();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void close() {
        try {
            c.close();			// Cerrar la conexion con la BBDD
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
