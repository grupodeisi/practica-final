import java.sql.*;
import java.util.ArrayList;
import java.util.List;

//NOS EVITA USAR SQL, ES DECIR, LO QUE ESTÁ DETRÁS DE CUANDO PROBEMOS

public class AsignaturaDao {

    private static Connection c;

    public AsignaturaDao() {
        try {
            if(c!=null) return;

            c = DriverManager.getConnection("jdbc:sqlite:BBDD_pf.db");
            c.setAutoCommit(false); 

            //c.prepareStatement("CREATE TABLE IF NOT EXISTS Asignatura (id_asignatura INT NOT NULL, nombre CHAR (30) NOT NULL, ano INT NOT NULL, PRIMARY KEY (id_asignatura));").execute();
            c.commit();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public List<Asignatura> all() {

        List<Asignatura> allAsignaturas = new ArrayList<Asignatura>();

        try {
            PreparedStatement ps = c.prepareStatement("select * from Asignatura");

            ResultSet rs = ps.executeQuery();

            while(rs.next()) {
            	Integer id_asigntara = rs.getInt("id_asignatura");
                String name = rs.getString("nombre");
		Integer ano = rs.getInt("ano");
                allAsignaturas.add(new Asignatura(id_asigntara, name, ano));
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            return allAsignaturas;
        }
    }

    public void save(Asignatura asignatura) {
        try {
            PreparedStatement ps = c.prepareStatement("insert into Asignatura (id_asignatura, nombre, ano) values (?,?,?)");
            ps.setInt(1, asignatura.getId_asignatura());
            ps.setString(2, asignatura.getName());
            ps.setInt(3, asignatura.getAno());

            ps.execute();

            c.commit();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void close() {
        try {
            c.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
