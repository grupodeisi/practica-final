import java.sql.*;
import java.util.ArrayList;
import java.util.List;


public class AlumnoDao {

    private static Connection c;

    public AlumnoDao() {
        try {
            if(c!=null) return;

            c = DriverManager.getConnection("jdbc:sqlite:BBDD_pf.db");
            c.setAutoCommit(false);

            //c.prepareStatement("CREATE TABLE IF NOT EXISTS Alumno (nif CHAR(10) NOT NULL, nombre CHAR (30) NOT NULL, ano INT NOT NULL, PRIMARY KEY (nif));").execute();
            c.commit();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public List<Alumno> all() {

        List<Alumno> allAlumno = new ArrayList<Alumno>();

        try {
            PreparedStatement ps = c.prepareStatement("select * from Alumno");

            ResultSet rs = ps.executeQuery();

            while(rs.next()) {
                String nif = rs.getString("nif");
                String nombre = rs.getString("nombre");
                int ano = rs.getInt("ano");
                allAlumno.add(new Alumno(nif, nombre, ano));
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            return allAlumno;
        }
    }

    public void save(Alumno a) {
        try {
            PreparedStatement ps = c.prepareStatement("insert into Alumno (nif, nombre, ano) values (?,?,?)");
            ps.setString(1, a.getNif());
            ps.setString(2, a.getNombre());
            ps.setInt(3, a.getAno());

            ps.execute();

            c.commit();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void close() {
        try {
            c.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
