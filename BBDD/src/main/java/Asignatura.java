import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class Asignatura {
	
	private Integer id_asignatura;
	private String name;
	private Integer ano;

	public Asignatura(Integer id_asignatura, String name, int ano) {
		this.id_asignatura = id_asignatura;
		this.name = name;
		this.ano = ano;
	}
	
	public int getId_asignatura() {
		return id_asignatura;
	}

	public String getName() {
		return name;
	}

	public int getAno() {
		return ano;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		Asignatura asignatura = (Asignatura) o;

		if (Integer.compare(asignatura.id_asignatura, id_asignatura) != 0)
			return false;	
		//if (String.compare(asignatura.name, name) != 0)
		//	return false;
		if (Integer.compare(asignatura.ano, ano) != 0)
			return false;
		return name != null ? name.equals(asignatura.name) : asignatura.name == null;
	}

	@Override
	public int hashCode() {
		int result;
		//long temp;
		//result = ano != null ? ano.hashCode();
		//temp = Double.doubleToLongBits(value);
		result = id_asignatura.hashCode();
		return result;
	}
}
