public class Profesor {

	private String nif;
	private String nombre;

	public Profesor(String nif, String nombre) {
		this.nif = nif;
		this.nombre = nombre;
	}

	public String getNif() {
		return nif;
	}

	public String getNombre() {
		return nombre;
	}
	

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		Profesor prof = (Profesor) o;
	
		if (prof.nif.compareTo(nif) != 0)
			return false;
		//if (prof.nombre.compareTo(nombre))
		return nombre != null ? nombre.equals(prof.nombre) : prof.nombre == null;
	}

	@Override
	public int hashCode() {
		int result;
		//long temp;
		//result = nif != null ? nif.hashCode() : 0;
		//temp = Double.doubleToLongBits(value);
		//result = 31 * result + (int) (temp ^ (temp >>> 32));
		result = nif.hashCode();
		return result;
	}
}
