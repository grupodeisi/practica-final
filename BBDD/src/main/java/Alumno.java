public class Alumno {

	private String nif;
	private String nombre;
	private int ano;

	public Alumno(String nif, String nombre, int ano) {
		this.nif = nif;
		this.nombre = nombre;
		this.ano = ano;
	}

	public String getNif() {
		return nif;
	}

	public String getNombre() {
		return nombre;
	}
	
	public int getAno() {
		return ano;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		Alumno a = (Alumno) o;
	
		if (Integer.compare(a.ano, ano) != 0)
			return false;
		if (a.nif.compareTo(nif) != 0)
			return false;
			//return nif != null ? nif.equals(a.nif) : a.nif == null;
		//if (a.nombre.compareTo(nombre))
		return nombre != null ? nombre.equals(a.nombre) : a.nombre == null;
	}

	@Override
	public int hashCode() {
		int result;
		//long temp;
		//result = nif != null ? nif.hashCode() : 0;
		//temp = Double.doubleToLongBits(value);
		//result = 31 * result + (int) (temp ^ (temp >>> 32));
		result = nif.hashCode();
		return result;
	}
}
