public class Practica {

	private String nombre;
	private int id_asignatura;

	public Practica(String n, int id) {
		this.nombre = n;
		this.id_asignatura = id;
	}

	public String getNombre() {
		return nombre;
	}

	public int getIdAsignatura() {
		return id_asignatura;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		Practica p = (Practica) o;

		if (Integer.compare(p.id_asignatura, id_asignatura) != 0)
			return false;
		return nombre != null ? nombre.equals(p.nombre) : p.nombre == null;
	}

	@Override
	public int hashCode() {
		int result;
		result = nombre != null ? nombre.hashCode() : 0;
		return result + id_asignatura;
	}
}
