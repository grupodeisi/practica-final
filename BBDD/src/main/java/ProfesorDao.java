import java.sql.*;
import java.util.ArrayList;
import java.util.List;


public class ProfesorDao {

    private static Connection c;

    public ProfesorDao() {
        try {
            if(c!=null) return;

            c = DriverManager.getConnection("jdbc:sqlite:BBDD_pf.db");
            c.setAutoCommit(false);

            //c.prepareStatement("CREATE TABLE IF NOT EXISTS Profesor (nif CHAR(10) NOT NULL, nombre CHAR (30) NOT NULL, PRIMARY KEY (nif));").execute();
            c.commit();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public List<Profesor> all() {

        List<Profesor> allProfesor = new ArrayList<Profesor>();

        try {
            PreparedStatement ps = c.prepareStatement("select * from Profesor");

            ResultSet rs = ps.executeQuery();

            while(rs.next()) {
                String nif = rs.getString("nif");
                String nombre = rs.getString("nombre");
                allProfesor.add(new Profesor(nif, nombre));
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            return allProfesor;
        }
    }

    public void save(Profesor a) {
        try {
            PreparedStatement ps = c.prepareStatement("insert into Profesor (nif, nombre) values (?,?)");
            ps.setString(1, a.getNif());
            ps.setString(2, a.getNombre());

            ps.execute();

            c.commit();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void close() {
        try {
            c.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
