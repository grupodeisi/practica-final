import static org.junit.Assert.*;
import org.junit.*;
import java.util.*;

public class PracticaTest {

    @Before
    public void setUp(){
    	try{            
            Runtime rt = Runtime.getRuntime();
            Process proc = rt.exec("cp BBDD_inicializacion/BBDD_pf.db ./BBDD_pf.db");
        } catch (Throwable t)
          {
            t.printStackTrace();
          }
    }
    
    @Test
    public void PracticaTest1() {

        PracticaDao dao = new PracticaDao();
        Practica p1 = new Practica("Ejemplo 1", 01);
        Practica p2 = new Practica("Ejemplo 2", 01);

        dao.save(p1);
        dao.save(p2);

	List<Practica> result = dao.all();
        assertEquals(p1, result.get(4));
        assertEquals(p2, result.get(5));
        assertEquals(6, result.size());

        dao.close();
    }
}
