import static org.junit.Assert.*;
import org.junit.*;
import java.util.*;

public class AsignaturaTest {

    @Before
    public void setUp(){
    	try{            
            Runtime rt = Runtime.getRuntime();
            Process proc = rt.exec("cp BBDD_inicializacion/BBDD_pf.db ./BBDD_pf.db");
        } catch (Throwable t)
          {
            t.printStackTrace();
          }
    }

    @Test
    public void test1() {
	/*Asignatura va a tener: 
		Id, año y nombre
	*/
        AsignaturaDao dao = new AsignaturaDao();
        Asignatura matematicas = new Asignatura(10, "matematicas", 2021);
        Asignatura fisica = new Asignatura(3, "fisica", 2019);

        dao.save(matematicas);
        dao.save(fisica);

        List<Asignatura> result = dao.all();

        assertEquals(matematicas, result.get(2));
        assertEquals(fisica, result.get(3));

        dao.close();
    }

}
