import static org.junit.Assert.*;
import org.junit.*;
import java.util.*;

public class ProfesorTest {

    @Before
    public void setUp(){
    	try{            
            Runtime rt = Runtime.getRuntime();
            Process proc = rt.exec("cp BBDD_inicializacion/BBDD_pf.db ./BBDD_pf.db");
        } catch (Throwable t)
          {
            t.printStackTrace();
          }
    }
   
    @Test
    public void Profesor() {

        ProfesorDao dao = new ProfesorDao();
        Profesor pedro = new Profesor("998751068", "pedro");

        dao.save(pedro);

        List<Profesor> result = dao.all();

        assertEquals(pedro, result.get(2));

        dao.close();
    }

}
