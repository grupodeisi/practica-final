import static org.junit.Assert.*;
import org.junit.*;
import java.util.*;


public class AlumnoTest {
      
    @Before
    public void setUp(){
    	try{            
            Runtime rt = Runtime.getRuntime();
            Process proc = rt.exec("cp BBDD_inicializacion/BBDD_pf.db ./BBDD_pf.db");
        } catch (Throwable t)
          {
            t.printStackTrace();
          }
    }

  
    @Test
    public void Alumno() {

        AlumnoDao dao = new AlumnoDao();
        Alumno manolo = new Alumno("698751368", "Manolo", 2020);
        Alumno sergio = new Alumno("542368197", "Sergio", 2020);

        dao.save(manolo);
        dao.save(sergio);

        List<Alumno> result = dao.all();

        assertEquals(manolo, result.get(4));
        assertEquals(sergio, result.get(5));
        

        dao.close();
    }

}
