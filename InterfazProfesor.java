import java.util.*;
import java.util.Scanner;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class InterfazProfesor {
	
	public static void interfazUsuario(){
		//System.out.println("Introduce su id de profesor, aseguresé de que sea correcto: ");
		//String id_profesor = System.console().readLine();
		System.out.println("¿Qué desea hacer?");
		System.out.println("1. Ver alumnos de una asignatura.");
		System.out.println("2. Ver las calificaciones de una asignatura.");
		System.out.println("3. Ver las calificaciones de un alumno.");
		System.out.println("4. Editar resultados de una entrega.");
		System.out.println("5. Añadir práctica a una asignatura.");
		System.out.println("6. Añadir alumno a una asigntaura.");
		System.out.println("7. Añadir nueva asignatura.");
		System.out.println("8. Añadir nuevo alumno.");
		
		System.out.println("Introduce su selección:");
		//return id_profesor;
		
	}
	
	public static void eleccionOpcion (int eleccion, String nif_profesor){
		switch (eleccion) {
			case 2:
				System.out.println("Introduce el id de la asignatura");
				String id = System.console().readLine();
				System.out.println("----------------------------------");
				verResultadosAsignatura(id);
				break;
			case 3:
				System.out.println("Introduce el id del alumno");
				String nif = System.console().readLine();
				System.out.println("----------------------------------");
				verResultadosAlumno(nif);
				break;
			case 5:
				System.out.println("Introduce el id de la asignatura");
				String id2 = System.console().readLine();
				System.out.println("Introduce el nombre de la práctica");
				String practica = System.console().readLine();
				System.out.println("----------------------------------");
				añadirPractica(id2, practica);
				break;
			case 4:
				System.out.println("Introduce la url de la entrega");
				String url_repo = System.console().readLine();
				System.out.println("Introduce la nueva nota");
				String nota = System.console().readLine();
				System.out.println("----------------------------------");
				editarResultadosAlumno(url_repo, nota);
				break;
			case 7:
				System.out.println("Introduce el nombre de la nueva Asignatura");
				String nombre_asignatura = System.console().readLine();
				System.out.println("Introduce el año de la asignatura");
				String ano_asignatura = System.console().readLine();
				System.out.println("Introduce el id de la nueva asignatura");
				String id_asignatura = System.console().readLine();
				System.out.println("----------------------------------");
				crearNuevaAsignatura(nombre_asignatura, ano_asignatura, id_asignatura, nif_profesor);
				break;
				
			case 8:
				System.out.println("Introduce el nombre del nuevo Alumno");
				String nombre_alumno = System.console().readLine();
				System.out.println("Introduce el nif del Alumno");
				String nif_alumno = System.console().readLine();
				System.out.println("Introduce el año del Alumno");
				String ano_alumno = System.console().readLine();
				System.out.println("----------------------------------");
				crearNuevoAlumno(nombre_alumno, nif_alumno, ano_alumno);
				break;
				
			case 6:
				System.out.println("Introduce el nif Alumno");
				String nif_alumno_2 = System.console().readLine();
				System.out.println("Introduce el id de la Asignatura");
				String id_asignatura_2 = System.console().readLine();
				System.out.println("----------------------------------");
				añadirAlumnoAAsignatura(nif_alumno_2, id_asignatura_2);
				break;
			
			case 1:
				System.out.println("Introduce el id de la asignatura");
				String id3 = System.console().readLine();
				System.out.println("----------------------------------");
				verAlumnosAsignatura(id3, nif_profesor);
				break;
			default:
				System.out.println("Inroduce una opción correcta");			
		}
	}
	
	public static void verResultadosAsignatura(String id){
		Connection connection = null;
		String codigo = "SELECT Asignatura.nombre, Practicas.id_asignatura, Alumno.nombre as nombre_alumno, Alumno.nif, Entrega.nombre as nombre_entrega, nota, url_repo FROM Entrega INNER JOIN Practicas ON Practicas.nombre = Entrega.nombre INNER JOIN Alumno ON Alumno.nif = Entrega.nif INNER JOIN Asignatura ON Asignatura.id_asignatura = Practicas.id_asignatura WHERE Practicas.id_asignatura = " + id;
		try
		{
		  // create a database connection
			connection = DriverManager.getConnection("jdbc:sqlite:BBDD/BBDD_pf.db");
			Statement statement = connection.createStatement();
			ResultSet rs = statement.executeQuery(codigo);
			boolean entrar = false;
			while(rs.next())
			{
				String id_asignatura = rs.getString("id_asignatura");
				if (id.compareTo(id_asignatura) == 0){
					// read the result set
					System.out.println("Nombre asignatura: " + rs.getString("nombre"));
					System.out.println("ID asignatura: " + rs.getInt("id_asignatura"));
					System.out.println("Nombre alumno: " + rs.getString("nombre_alumno"));
					System.out.println("nif alumno: " + rs.getString("nif"));
					System.out.println("Nombre entrega: " + rs.getString("nombre_entrega"));
					System.out.println("Nota de la entrega: " + rs.getInt("nota"));
					System.out.println("URL Repositorio: " + rs.getString("url_repo"));
					System.out.println("----------------------------------");
					entrar = true;
				}
			}
			if(entrar == false){
				System.out.println("Esa asignatura no está registrada, o no tiene calificaciones guardadas.");
			}
		}
		catch(SQLException e)
		{
			// if the error message is "out of memory",
			// it probably means no database file is found
			System.err.println(e.getMessage());
		}
		finally
		{
			try
			{
			if(connection != null)
				connection.close();
			}
			catch(SQLException e)
			{
				// connection close failed.
				System.err.println(e.getMessage());
			}
		}
	}
	
	public static void verResultadosAlumno(String nif){
		Connection connection = null;
		String codigo = "SELECT Alumno.nombre as nombre_alumno, Alumno.nif as nif_alumno, Entrega.nombre as nombre_entrega, nota, url_repo, Asignatura.nombre as nombre_asignatura FROM Entrega INNER JOIN Practicas ON Practicas.nombre = Entrega.nombre INNER JOIN Alumno ON nif_alumno = Entrega.nif INNER JOIN Asignatura ON Asignatura.id_asignatura = Practicas.id_asignatura WHERE Alumno.nif = " + nif;
		try
		{
		  // create a database connection
			connection = DriverManager.getConnection("jdbc:sqlite:BBDD/BBDD_pf.db");
			Statement statement = connection.createStatement();
			ResultSet rs = statement.executeQuery(codigo);
			boolean entrar = false;
			while(rs.next())
			{
				String nif_alumno = rs.getString("nif_alumno");
				if (nif.compareTo(nif_alumno) == 0){
					// read the result set
					System.out.println("Nombre alumno: " + rs.getString("nombre_alumno"));
					System.out.println("nif alumno: " + rs.getString("nif_alumno"));
					System.out.println("Nombre entrega: " + rs.getString("nombre_entrega"));
					System.out.println("Nota de la entrega: " + rs.getInt("nota"));
					System.out.println("URL Repositorio: " + rs.getString("url_repo"));
					System.out.println("Nombre Asignatura: " + rs.getString("nombre_asignatura"));
					System.out.println("----------------------------------");
					entrar = true;
				}
			}
			if(entrar == false){
				System.out.println("El alumno no está registrado o no ha realizado ninguna entrega");
			}
		}
		catch(SQLException e)
		{
			// if the error message is "out of memory",
			// it probably means no database file is found
			System.err.println(e.getMessage());
		}
		finally
		{
			try
			{
			if(connection != null)
				connection.close();
			}
			catch(SQLException e)
			{
				// connection close failed.
				System.err.println(e.getMessage());
			}
		}
	}
	
	public static void verAlumnosAsignatura(String id, String nif_profesor){
		Connection connection = null;
		Connection connection2 = null;
		String codigo = "SELECT Asignatura.nombre as nombre_asigntatura, asignatura_alumno.id_asignatura, Alumno.nombre as nombre_alumno, asignatura_alumno.nif FROM asignatura_alumno INNER JOIN Alumno ON Alumno.nif = asignatura_alumno.nif INNER JOIN Asignatura ON Asignatura.id_asignatura = asignatura_alumno.id_asignatura WHERE Asignatura.id_asignatura = " + id;
		try
		{
		  // create a database connection
		  	connection2 = DriverManager.getConnection("jdbc:sqlite:BBDD/BBDD_pf.db");
			Statement statement2 = connection2.createStatement();
			ResultSet rs2 = statement2.executeQuery("select * from asignatura");
			boolean asignatura_valida = false;
			while(rs2.next())
			{
				String id_asignatura = rs2.getString("id_asignatura");
				if (id.compareTo(id_asignatura) == 0){
					asignatura_valida = true;
				}
			}
			connection = DriverManager.getConnection("jdbc:sqlite:BBDD/BBDD_pf.db");
			Statement statement = connection.createStatement();
			ResultSet rs = statement.executeQuery(codigo);
			int i = 1;
			
			if (asignatura_valida == true){
				System.out.println("Alumnos pertenecientes a la asignatura " + rs.getString("nombre_asigntatura") + ":");
				while(rs.next())
				{
					// read the result set
					System.out.print(i + "- " + rs.getString("nombre_alumno"));
					System.out.println(", nif: " + rs.getString("nif"));
					i = i + 1;
				}
			}
			else{
				System.out.println("Esta asignatura no está registrada. Si quiere registrala escriba 'registrar'");
				String registrar = System.console().readLine();
				if (registrar.compareTo("registrar") == 0){
					eleccionOpcion (5, nif_profesor);
				}
			}
		}
		catch(SQLException e)
		{
			// if the error message is "out of memory",
			// it probably means no database file is found
			System.err.println(e.getMessage());
		}
		finally
		{
			try
			{
			if(connection != null)
				connection.close();
			}
			catch(SQLException e)
			{
				// connection close failed.
				System.err.println(e.getMessage());
			}
		}
	}
	
	
	public static void añadirPractica(String id, String practica){
		Connection connection = null;
		String codigo = "INSERT INTO 'Practicas' (nombre, id_asignatura) VALUES ('" + practica + "', " + id + ")";	
		try
		{
		  // create a database connection
			connection = DriverManager.getConnection("jdbc:sqlite:BBDD/BBDD_pf.db");
			Statement statement = connection.createStatement();
			ResultSet rs = statement.executeQuery("select * from practicas");
			boolean repetida = false;
			boolean id_valido = false;
			while(rs.next())
			{
				String nombre_practica = rs.getString("nombre");
				String id_asignatura = rs.getString("id_asignatura");
				if (practica.compareTo(nombre_practica) == 0){
					repetida = true;
				}
				if (id.compareTo(id_asignatura) == 0){
					id_valido = true;
				} 
			}
			if (repetida == false && id_valido == true){
				statement.executeUpdate​(codigo);
				System.out.println("La práctica se ha añadido satisfactoriamente");
			}else{
				System.out.println("El nombre de la práctica ya está registrado o la asignatura no está registrada");
			}
		}
		catch(SQLException e)
		{
			// if the error message is "out of memory",
			// it probably means no database file is found
			System.err.println(e.getMessage());
		}
		finally
		{
			try
			{
			if(connection != null)
				connection.close();
			}
			catch(SQLException e)
			{
				// connection close failed.
				System.err.println(e.getMessage());
			}
		}
	}
	
	public static void editarResultadosAlumno(String url_repo, String nota) {
		Connection connection = null;
		String codigo = "UPDATE Entrega SET nota = " + nota + " WHERE url_repo = " + "'" + url_repo + "'";
		try
		{
		  // create a database connection
			connection = DriverManager.getConnection("jdbc:sqlite:BBDD/BBDD_pf.db");
			Statement statement = connection.createStatement();
			ResultSet rs = statement.executeQuery("select * from entrega");
			boolean entra = false;
			while(rs.next())
			{
				String url_entrega = rs.getString("url_repo");
				if (url_repo.compareTo(url_entrega) == 0){
					entra = true;
				}
			}
			if (entra == true){
				statement.executeUpdate​(codigo);
				System.out.println("El cambio se ha realizado satisfactoriamente");
			}else{
				System.out.println("La url de la entrega no está registrada");
			}
			
		}
		catch(SQLException e)
		{
			// if the error message is "out of memory",
			// it probably means no database file is found
			System.err.println(e.getMessage());
		}
		finally
		{
			try
			{
			if(connection != null)
				connection.close();
			}
			catch(SQLException e)
			{
				// connection close failed.
				System.err.println(e.getMessage());
			}
		}
	}
	
	public static void crearNuevaAsignatura(String nombre, String ano, String id, String nif_profesor) {
		Connection connection = null;
		String codigo = "INSERT INTO 'Asignatura' (id_asignatura, nombre, ano) VALUES (" + id + ", '" + nombre + "', " + ano + ")";
		String codigo2 = "INSERT INTO 'asignatura_profesor' (nif, id_asignatura) VALUES ('" + nif_profesor + "', " + id + ")";
		try
		{
		  // create a database connection
			connection = DriverManager.getConnection("jdbc:sqlite:BBDD/BBDD_pf.db");
			Statement statement = connection.createStatement();
			ResultSet rs = statement.executeQuery("select * from asignatura");
			boolean repetida = false;
			while(rs.next())
			{
				String id_asignatura = rs.getString("id_asignatura");
				if (id.compareTo(id_asignatura) == 0){
					repetida = true;
				}
			}
			if (repetida == false){
				statement.executeUpdate​(codigo);
				statement.executeUpdate​(codigo2);
				System.out.println("La asignatura se ha añadido satisfactoriamente");
			}else{
				System.out.println("El id pertenece a otra asignatura");
			}
		}
		catch(SQLException e)
		{
			// if the error message is "out of memory",
			// it probably means no database file is found
			System.err.println(e.getMessage());
		}
		finally
		{
			try
			{
			if(connection != null)
				connection.close();
			}
			catch(SQLException e)
			{
				// connection close failed.
				System.err.println(e.getMessage());
			}
		}
	}
	
	public static void crearNuevoAlumno(String nombre, String nif, String ano) {
		Connection connection = null;
		String codigo = "INSERT INTO 'Alumno' (nif, nombre, ano) VALUES (" + nif + ", '" + nombre + "', " + ano + ")";
		try
		{
		  // create a database connection
			connection = DriverManager.getConnection("jdbc:sqlite:BBDD/BBDD_pf.db");
			Statement statement = connection.createStatement();
			ResultSet rs = statement.executeQuery("select * from alumno");
			boolean repetida = false;
			while(rs.next())
			{
				String nif_alumno = rs.getString("nif");
				if (nif.compareTo(nif_alumno) == 0){
					repetida = true;
				}
			}
			if (repetida == false){
				statement.executeUpdate​(codigo);
				System.out.println("El alumno se ha añadido satisfactoriamente");
			}else{
				System.out.println("El id pertenece a otro alumno");
			}			
		}
		catch(SQLException e)
		{
			// if the error message is "out of memory",
			// it probably means no database file is found
			System.err.println(e.getMessage());
		}
		finally
		{
			try
			{
			if(connection != null)
				connection.close();
			}
			catch(SQLException e)
			{
				// connection close failed.
				System.err.println(e.getMessage());
			}
		}
	}
	
	public static void añadirAlumnoAAsignatura(String nif, String id_asignatura) {
		Connection connection = null;
		Connection connection2 = null;
		String codigo = "INSERT INTO 'asignatura_alumno' (nif, id_asignatura) VALUES (" + nif + ", " + id_asignatura + ")";
		try
		{
		  // create a database connection
			connection = DriverManager.getConnection("jdbc:sqlite:BBDD/BBDD_pf.db");
			connection2 = DriverManager.getConnection("jdbc:sqlite:BBDD/BBDD_pf.db");
			Statement statement = connection.createStatement();
			Statement statement2 = connection.createStatement();
			ResultSet rs = statement.executeQuery("select * from alumno");
			ResultSet rs2 = statement2.executeQuery("select * from asignatura");
			boolean entra = false;
			boolean id_valido = false;
			while(rs.next())
			{
				String nif_alumno = rs.getString("nif");
				if (nif.compareTo(nif_alumno) == 0){
					entra = true;
				}
			}
			while(rs2.next())
			{
				String id = rs2.getString("id_asignatura");
				if (id_asignatura.compareTo(id) == 0){
					id_valido = true;
				}
			}
			if (entra == true && id_valido == true){
				statement.executeUpdate​(codigo);
				System.out.println("El alumno se ha añadido satifactoriamente a la asignatura");
			}else{
				System.out.println("El alumno y/o la asignatura no están registrado/s");
			}
			
			
		}
		catch(SQLException e)
		{
			// if the error message is "out of memory",
			// it probably means no database file is found
			System.err.println(e.getMessage());
		}
		finally
		{
			try
			{
			if(connection != null)
				connection.close();
			}
			catch(SQLException e)
			{
				// connection close failed.
				System.err.println(e.getMessage());
			}
		}
	}
	
	public static boolean profesorExiste(String nif){
		Connection connection = null;
		String codigo = "select * from profesor";
		try
		{
		  // create a database connection
			connection = DriverManager.getConnection("jdbc:sqlite:BBDD/BBDD_pf.db");
			Statement statement = connection.createStatement();
			ResultSet rs = statement.executeQuery(codigo);
			boolean existe = false;
			while(rs.next())
			{
				String nif_profesor = rs.getString("nif");
				if (nif.compareTo(nif_profesor) == 0){
					existe = true;
				}
			}
			if (existe == true){
				return true;
			}else{
				return false;
			}			
		}
		catch(SQLException e)
		{
			// if the error message is "out of memory",
			// it probably means no database file is found
			System.err.println(e.getMessage());
		}
		finally
		{
			try
			{
			if(connection != null)
				connection.close();
			}
			catch(SQLException e)
			{
				// connection close failed.
				System.err.println(e.getMessage());
			}
		}
		return false;
	}
	
	public static void crearProfesor(String nif, String nombre){
		Connection connection = null;
		String codigo = "INSERT INTO 'Profesor' (nif, nombre) VALUES ('" + nif + "', '" + nombre + "')";
		try
		{
		  // create a database connection
			connection = DriverManager.getConnection("jdbc:sqlite:BBDD/BBDD_pf.db");
			Statement statement = connection.createStatement();
			ResultSet rs = statement.executeQuery("select * from profesor");
			boolean disponible = true;
			while(rs.next())
			{
				String nif_profesor = rs.getString("nif");
				if (nif.compareTo(nif_profesor) == 0){
					disponible = false;
				}
			}
			if (disponible == true){
				statement.executeUpdate​(codigo);
				System.out.println("El profesor se ha añadido satifactoriamente");
			}else{
				System.out.println("El nif no es válido");
			}
						
		}
		catch(SQLException e)
		{
			// if the error message is "out of memory",
			// it probably means no database file is found
			System.err.println(e.getMessage());
		}
		finally
		{
			try
			{
			if(connection != null)
				connection.close();
			}
			catch(SQLException e)
			{
				// connection close failed.
				System.err.println(e.getMessage());
			}
		}
	}
	

	public static void main(String[] args) {
		System.out.println("Bienvenido profesor, introduzca su nif: ");
		String nif_profesor = System.console().readLine();	
		if (profesorExiste(nif_profesor) == true){
			interfazUsuario();	
			int eleccion = Integer.parseInt(System.console().readLine());
			eleccionOpcion(eleccion, nif_profesor);
		}else{
			System.out.println("Actualmente no hay ningún profesor con ese nif. Si desea introducirse a la BBDD escribe 'registrarse':");
			String registrar = System.console().readLine();
				if (registrar.compareTo("registrarse") == 0){
					System.out.println("Introduzca su nombre:");
					String nombre_profesor = System.console().readLine();
					crearProfesor(nif_profesor, nombre_profesor);
				}
		}
		
	}

}
