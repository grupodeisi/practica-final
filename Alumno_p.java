 import java.sql.Connection;
 import java.sql.DriverManager;
 import java.sql.ResultSet;
 import java.sql.SQLException;
 import java.sql.Statement;

    public class Alumno_p
    {
      public static void main(String[] args)
      {
        Connection connection = null;
        try
        {
          System.out.println("Número de dni: ");
          String dni;
          String nif;
          String url;
          String nombre;
          Boolean Not_Found = true;//sin dni, no se puede entrar
          dni = System.console().readLine();
          System.out.println("URL entrega: ");
          url = System.console().readLine();
          System.out.println("Nombre entrega: ");
          nombre = System.console().readLine();
          // create a database connection
          connection = DriverManager.getConnection("jdbc:sqlite:BBDD/BBDD_pf.db");
          Statement statement = connection.createStatement();
          ResultSet rs = statement.executeQuery("select * from Alumno");
          String http = "INSERT INTO 'ENTREGA' (nombre, nif, url_repo, nota) VALUES ('pp4'," + dni + ",'" + url + "', 7.0)";

	  while(rs.next()){
	  	nif  = rs.getString("nif");
	  	if (dni.compareTo(nif) == 0){
			//read the result set
			System.out.println("----------------------------------------------");
			System.out.println("Datos del alumn@ cuyo dni es " + dni + " para realizar su entrega: ");
			System.out.println(" ");
			System.out.println("Nombre de la práctica: " + nombre);
			System.out.println("DNI del alumno: " + rs.getString("nif"));
			System.out.println("URL_repo: " + url);
			System.out.println("----------------------------------------------");
          		statement.executeUpdate(http);
			Not_Found = false;
		}
	  }
	  if (Not_Found){//!Not_Found->caso contrario
		  System.out.println(" ");
		  System.out.println("Número de dni no válido. Por favor, introduzca un número válido.");
	  }
        }
        catch(SQLException e)
        {
          // if the error message is "out of memory",
          // it probably means no database file is found
          System.err.println(e.getMessage());
        }
        finally
        {
          try
          {
            if(connection != null)
              connection.close();
          }
          catch(SQLException e)
          {
            // connection close failed.
            System.err.println(e.getMessage());
          }
        }
      }
    }
