package urjc.isi.dao_mocked;

import java.io.File;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.InvalidRemoteException;
import org.eclipse.jgit.api.errors.TransportException;
import org.eclipse.jgit.lib.ObjectReader;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.treewalk.CanonicalTreeParser;
import org.eclipse.jgit.diff.DiffEntry;

import java.util.*;

// Import log4j classes.
import org.apache.log4j.Logger;
import org.apache.log4j.BasicConfigurator;

public class DetectaCopias {
    public static void main(String[] args) throws  InvalidRemoteException, TransportException, GitAPIException {
	
	System.out.println("hola");
	
	//Clono repositorio 1
	Git git1 = Git.cloneRepository()
	    .setURI("https://github.com/mininet/mininet.git")
	    .setDirectory(new File("/tmp/k1"))
	    .call();
	
	//Clono repositorio 2
	Git git2 = Git.cloneRepository()
	    .setURI("https://gitlab.etsit.urjc.es/grupodeisi/practica-final")
	    .setDirectory(new File("/tmp/k2"))
	    .call();
	
	//Obtengo el último commit del repo 1
	Iterable<RevCommit> log1 = git1.log().call();
	RevCommit c1 = null;
	for (RevCommit commit : log1) {
		c1 = commit;
	}
		/*//Show last commit
		System.out.println("LogCommit: " + c1);
		String logMessage = c1.getFullMessage();
		System.out.println("LogMessage: " + logMessage);*/

	//Obtengo el último commit del repo 2
	Iterable<RevCommit> log2 = git2.log().call();
	RevCommit c2 = null;
	for (RevCommit commit : log2) {
		c2 = commit;
	}
	
	//Creo los respositorios
	Repository repo1 = git1.getRepository();
	Repository repo2 = git2.getRepository();
	
	//Differencias entre los dos commits
	ObjectReader reader1 = repo1.newObjectReader();
	ObjectReader reader2 = repo2.newObjectReader();

	try{            
            	CanonicalTreeParser oldTreeIter = new CanonicalTreeParser();
		oldTreeIter.reset(reader1, c1);
		CanonicalTreeParser newTreeIter = new CanonicalTreeParser();
		newTreeIter.reset(reader2, c2);

		System.out.println("Differences between two commits: ");

		List<DiffEntry> diffs = git1.diff().setOldTree(oldTreeIter).setNewTree(newTreeIter).call();

		for (DiffEntry diff : diffs) {
		    System.out.println(diff.getPath(null));
		} 
        } catch (Throwable t){
            t.printStackTrace();
        }
	
	try{            
            Runtime rt = Runtime.getRuntime();
            Process proc1 = rt.exec("rm -r /tmp/k1");
            Process proc2 = rt.exec("rm -r /tmp/k2");
        } catch (Throwable t){
            t.printStackTrace();
        }
    }	
    
}
