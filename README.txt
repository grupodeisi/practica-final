GUÍA PARA EJECUTAR LA PRÁCTICA:

* BBDD
La BBDD se genera a partir de un archivo sql en limpio (main/BBDD/BBDD_inicializacion). Las pruebas que se realizan sobre la BBDD se hacen partiendo de esta BBDD limpia. Para ejecutar las pruebas de la BBDD:
	cd BBDD
	mvn test



* DETECTAR COPIAS
Programa que partiendo de dos proyectos git proporcionados como urls indica sus diferencias. Para compilar el programa:
	cd DetectarCopias
	mvn package
	cd target
	java -cp dao_mocked-0.0.1-SNAPSHOT-jar-with-dependencies.jar urjc.isi.dao_mocked.DetectaCopias



* INTERFACES DE USUARIOS
* Interfaz alumno

Hay tres archivos:

    1. Listado_notas que, como su nombre indica despliega una lista con las notas. Esta lista contiene exactamente el nombre de la práctica, el DNI del alumno, la URL de su entrega y la nota asociada a esta.

    2. Alumno es la interfaz que permite al estudiante consultar su calificación usándo su DNI como clave de acceso. Una vez introducido el DNI de forma correcta (si no nos dará un mensaje de número inválido) nos aparecerán nuestras entregas con el nombre de la práctica, nuestro DNI (cosa innecesaria), URL de la entrega y la nota.

    3. Alumno_p es donde el alumno puede realizar entregas. Nos piden el DNI, la URL y el nombre de la entrega. Si el DNI no está en la BBDD nos salrá el mismo mensaje de antes. Si el DNI es correcto los datos se guardarán y se mostrarán en la pantalla.


* Interfaz profesor

Nos aparecerá un menú con 7 opciones. Si introducimos otra opción el programa nos pedirá que introduzcamos una válida.













